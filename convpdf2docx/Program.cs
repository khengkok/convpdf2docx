﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acrobat;
using System.Reflection;
using System.Globalization;
using System.IO;

namespace convpdf2docx
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: convpdf2docx <src_dir> <dst_dir>");
                return;
            }
            string srcDirectory = args[0];
            string destDirectory = args[1];

            string[] fileArray = Directory.GetFiles(args[0], "*.pdf");
            foreach (string file in fileArray)
            {
                AcroPDDoc pdfd = new AcroPDDoc();
                pdfd.Open(file);
                //  pdfd.Open(sourceDoc.FileFullPath);
                Object jsObj = pdfd.GetJSObject();
                Type jsType = pdfd.GetType();
                //have to use acrobat javascript api because, acrobat
                string fname = Path.GetFileName(file);
                string destfname = fname.Split('.')[0] + ".docx";
                string destPath = Path.Combine(destDirectory, destfname);
                object[] saveAsParam = { destPath, "com.adobe.acrobat.docx", "", false, false };
                Console.WriteLine("Converting " + file);
                jsType.InvokeMember("saveAs", BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance, null, jsObj, saveAsParam, CultureInfo.InvariantCulture);
                pdfd.Close();
            }
        }
    }
}
